package uex.asee.cajas;

import android.content.Context;

import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.Cajas.BoxesNetworkDataSource;
import uex.asee.cajas.remote.Cajas.BoxesRepository;
import uex.asee.cajas.remote.Casas.HousesNetworkDataSource;
import uex.asee.cajas.remote.Casas.HousesRepository;
import uex.asee.cajas.remote.Objetos.ItemsNetworkDataSource;
import uex.asee.cajas.remote.Objetos.ItemsRepository;
import uex.asee.cajas.ui.BoxesModelFactory;
import uex.asee.cajas.ui.HousesModelFactory;
import uex.asee.cajas.ui.ItemsModelFactory;

public class AppContainer {
    private InventarioDatabase database;
    private HousesNetworkDataSource housesNetworkDataSource;
    public HousesRepository housesRepository;
    public HousesModelFactory housesfactory;

    private ItemsNetworkDataSource itemsNetworkDataSource;
    public ItemsRepository itemsRepository;
    public ItemsModelFactory itemsfactory;

    private BoxesNetworkDataSource boxesNetworkDataSource;
    public BoxesRepository boxesRepository;
    public BoxesModelFactory boxesfactory;

    //1-Contiene como públicos los objetos que se necesitan para el funcionamiento de la aplicación con
// todas sus dependencias inyectadas
    public AppContainer(Context context){
        database = InventarioDatabase.getInstance(context); //Fuente de datos local

        housesNetworkDataSource = HousesNetworkDataSource.getInstance(); //Fuente de datos remota
        housesRepository = HousesRepository.getInstance(database.getDaoCasa(), housesNetworkDataSource);
        housesfactory = new HousesModelFactory(housesRepository);

        itemsNetworkDataSource = ItemsNetworkDataSource.getInstance();
        itemsRepository = ItemsRepository.getInstance(database.getDaoObjeto(), itemsNetworkDataSource);
        itemsfactory = new ItemsModelFactory(itemsRepository);

        boxesNetworkDataSource= BoxesNetworkDataSource.getInstance();
        boxesRepository = BoxesRepository.getInstance(database.getDaoCaja(),boxesNetworkDataSource);
        boxesfactory = new BoxesModelFactory(boxesRepository);
    }
}
