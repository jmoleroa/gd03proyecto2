package uex.asee.cajas.remote.Cajas;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uex.asee.cajas.Actividades.Login;
import uex.asee.cajas.Adapters.OnBoxesLoadedListener;
import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Interfaces.BoxAPI;
import uex.asee.cajas.Models.Box;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.remote.RetrofitClient;


public class BoxesNetworkRunnable implements Runnable{
    private final OnBoxesLoadedListener mOnBoxesLoadedListener;


    public BoxesNetworkRunnable(OnBoxesLoadedListener onBoxesLoadedListener){
        mOnBoxesLoadedListener = onBoxesLoadedListener;

    }

    @Override
    public void run() {
        BoxAPI boxAPI = RetrofitClient.getClient().create(BoxAPI.class);
        Call<List<Box>> call = boxAPI.getBoxes(Login.TOKEN.getToken());
        final List<Caja> boxes =  new ArrayList<>() ;
        call.enqueue(new Callback<List<Box>>() {
            @Override
            public void onResponse(Call<List<Box>> call, Response<List<Box>> response) {
                if(response.isSuccessful()){
                    for(Box caja:response.body()){
                        boxes.add(new Caja(caja.get_id(),caja.getName(),caja.getIdcasa(), caja.getDescription(), caja.getUserid(),caja.getImginfo()));
                    }

                    AppExecutors.getInstance().mainThread().execute(() -> mOnBoxesLoadedListener.onBoxesLoaded(boxes));
                }


            }
            @Override
            public void onFailure(Call<List<Box>> call, Throwable t) {

            }
        });


    }

}
