package uex.asee.cajas.remote.Casas;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.remote.AppExecutors;

public class HousesNetworkDataSource {
    private static final String LOG_TAG = HousesNetworkDataSource.class.getSimpleName();
    private static HousesNetworkDataSource sInstance;

    // LiveData storing the latest downloaded weather forecasts
    private final MutableLiveData<List<Casa>> mDownloadedCasas;

    private HousesNetworkDataSource() {
        mDownloadedCasas = new MutableLiveData<>();
    }

    public synchronized static HousesNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new HousesNetworkDataSource();
            Log.d(LOG_TAG, "Made new network data source");
        }
        return sInstance;
    }

    public LiveData<List<Casa>> getCurrentHouses() {
        return mDownloadedCasas;
    }

    /**
     * Gets the newest repos
     */
    public void fetchHouses() {
        Log.d(LOG_TAG, "Fetch repos started");
        // Get gata from network and pass it to LiveData
        AppExecutors.getInstance().networkIO().execute(new HousesNetworkRunnable(mDownloadedCasas::postValue));
    }

    public void setFromDatabase(List<Casa> casas){
        mDownloadedCasas.postValue(casas);
    }
}
