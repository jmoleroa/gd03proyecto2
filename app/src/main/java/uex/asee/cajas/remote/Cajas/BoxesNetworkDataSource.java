package uex.asee.cajas.remote.Cajas;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.remote.AppExecutors;

public class BoxesNetworkDataSource {
    private static final String LOG_TAG = BoxesNetworkDataSource.class.getSimpleName();
    private static BoxesNetworkDataSource sInstance;

    // LiveData storing the latest downloaded weather forecasts
    private final MutableLiveData<List<Caja>> mDownloadedCajas;

    private BoxesNetworkDataSource() {
        mDownloadedCajas = new MutableLiveData<>();
    }

    public synchronized static BoxesNetworkDataSource getInstance() {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new BoxesNetworkDataSource();
            Log.d(LOG_TAG, "Made new network data source");
        }
        return sInstance;
    }

    public LiveData<List<Caja>> getCurrentCajas() {
        return mDownloadedCajas;
    }


    /**
     * Gets the newest repos
     */
    public void fetchCajas() {
        Log.d(LOG_TAG, "Fetch repos started");
        // Get gata from network and pass it to LiveData
        AppExecutors.getInstance().networkIO().execute(new BoxesNetworkRunnable(mDownloadedCajas::postValue));
    }

    public void setFromDatabase(List<Caja> cajas){
        mDownloadedCajas.postValue(cajas);
    }
}
