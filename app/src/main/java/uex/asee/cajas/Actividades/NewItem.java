package uex.asee.cajas.Actividades;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import uex.asee.cajas.AppContainer;
import uex.asee.cajas.Entidades.Casa;
import uex.asee.cajas.remote.AppExecutors;
import uex.asee.cajas.Entidades.Caja;
import uex.asee.cajas.Entidades.Objeto;
import uex.asee.cajas.Interfaces.ItemAPI;
import uex.asee.cajas.Models.Item;
import uex.asee.cajas.R;
import uex.asee.cajas.Roomdb.InventarioDatabase;
import uex.asee.cajas.remote.RetrofitClient;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uex.asee.cajas.ui.BoxesViewModel;
import uex.asee.cajas.ui.HousesViewModel;

public class NewItem extends AppCompatActivity {
    Spinner spinner;
    Spinner cajaSpinner;
    ArrayList<String> cajasNames= new ArrayList<String>();
    ArrayList<Caja> boxes= new ArrayList<Caja>();
    ImageView imageView;
    File photo;
    Uri auxuri;
    String idCaja;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_item);
        imageView = findViewById(R.id.prev_new_box_image);

        ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
                new ActivityResultCallback<Uri>() {
                    @SuppressLint("Range")
                    @Override
                    public void onActivityResult(Uri uri) {
                        System.out.println(uri);
                        imageView.setImageURI(uri);
                        auxuri = uri;
                    }
                });
        Button btnChooseFile = findViewById(R.id.button_new_select_photo);
        btnChooseFile.setOnClickListener(view -> {
            mGetContent.launch("image/*");
        });

        final EditText nItemName = (EditText) findViewById(R.id.nitem_name);
        final EditText nItemDescription = (EditText) findViewById(R.id.nitem_description);
        spinner = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(this, R.array.categorias, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinner.setAdapter(adapter);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        cajaSpinner=findViewById(R.id.spinnerbox);
        AppContainer appContainer = MainActivity.appContainer;

        BoxesViewModel mViewModel = new ViewModelProvider(this, appContainer.boxesfactory).get(BoxesViewModel.class);

        mViewModel.getBoxes().observe(this, cajas -> {

            if (cajas != null) {
                boxes = (ArrayList<Caja>) cajas;
                System.out.println("TAMA´ÑO CASAS " + boxes.size());
                for (Caja box : boxes) {
                    cajasNames.add(box.getName());

                }
                System.out.println("TAMAÑO CASASNOM " + cajasNames.size());
            }
            ArrayAdapter<String> adapter2=new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, cajasNames);
            cajaSpinner.setAdapter(adapter2);
            cajaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (spinner.getSelectedItemPosition() > -1){
                        idCaja = boxes.get(spinner.getSelectedItemPosition()).getId();
                        System.out.println("HOLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final EditText nItemName = (EditText) findViewById(R.id.nitem_name);
        final EditText nItemDescription = (EditText) findViewById(R.id.nitem_description);
        switch (item.getItemId()) {
            case R.id.action_cancel:
                this.finish();
                return true;
            case R.id.action_confirm:
                System.out.println("valor spinner " +cajaSpinner.getSelectedItem() + " posicion " +spinner.getSelectedItemPosition());
                String categoria = spinner.getSelectedItem().toString();
                String idCaja = null;
                if (cajaSpinner.getSelectedItemPosition() > -1){
                    idCaja = boxes.get(cajaSpinner.getSelectedItemPosition()).getId();
                }

                System.out.println("categoria " +categoria + " id caja " + idCaja);
                if(!nItemName.getText().toString().isEmpty() && !nItemDescription.getText().toString().isEmpty()){
                    createItem(nItemName.getText().toString(),nItemDescription.getText().toString(),idCaja,categoria);
                    this.finish();
                    return true;
                }else{
                    Toast.makeText(NewItem.this, "Por favor rellena todos los campos", Toast.LENGTH_SHORT).show();
                }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void createItem(String name,String description,String idcaja,String categoria){
        ItemAPI itemAPI = RetrofitClient.getClient().create(ItemAPI.class);
        RequestBody rname = RequestBody.create(MediaType.parse("text/plain"),name);
        RequestBody rdescription = RequestBody.create(MediaType.parse("text/plain"),description);
        RequestBody ridcaja = null;
        if(idcaja!= null){
            ridcaja = RequestBody.create(MediaType.parse("text/plain"),idcaja);
        }
        RequestBody rcategoria = RequestBody.create(MediaType.parse("text/plain"),categoria);
        RequestBody rimg = null;
        MultipartBody.Part rbimg = null;
        try {
            if(auxuri != null){
                InputStream is = getContentResolver().openInputStream(auxuri);
                if(is!= null){
                    rimg = RequestBody.create(
                            MediaType.parse(getContentResolver().getType(auxuri)),
                            getBytes(is)

                    );
                    MimeTypeMap mime = MimeTypeMap.getSingleton();
                    String type = mime.getExtensionFromMimeType(getContentResolver().getType(auxuri));

                    rbimg =  MultipartBody.Part.createFormData("image", name+"."+type, rimg);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Call<Item> call = itemAPI.createItem(Login.TOKEN.getToken(),rname,rdescription,rbimg,ridcaja,rcategoria);

        call.enqueue(new Callback<Item>() {
            @Override
            public void onResponse(Call<Item> call, Response<Item> response) {
                if(response.isSuccessful()){
                    String id = response.body().get_id();
                    String name =  response.body().getName();
                    String idCaja =  response.body().getIdcaja();
                    String description = response.body().getDescription();
                    String userId = response.body().getUserid();
                    String imagen = response.body().getImginfo();
                    String categoria = response.body().getCategoria();
                    if(imagen == null){
                        imagen = "";
                    }
                    if(idCaja == null){
                        idCaja = "";
                    }
                    if(description == null){
                        description = "";
                    }

                    Objeto item = new Objeto(id,name,categoria,imagen,description,idCaja,userId);
                    if(item != null){
                        AppExecutors.getInstance().diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                InventarioDatabase database = InventarioDatabase.getInstance(NewItem.this);
                                database.getDaoObjeto().insert(item);
                            }
                        });
                    }
                }else{
                    Toast.makeText(NewItem.this, "Error al crear caja", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Item> call, Throwable t) {
                System.out.println(t.getCause().toString());
                Toast.makeText(NewItem.this, "Error al conectarse con los servicios", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public byte[] getBytes(InputStream is) throws IOException {
        ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();

        int buffSize = 1024;
        byte[] buff = new byte[buffSize];

        int len = 0;
        while ((len = is.read(buff)) != -1) {
            byteBuff.write(buff, 0, len);
        }

        return byteBuff.toByteArray();
    }


}