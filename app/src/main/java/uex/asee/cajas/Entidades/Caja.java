package uex.asee.cajas.Entidades;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "caja")
public class Caja {
    @Ignore
    public static final String ITEM_SEP = System.getProperty("line.separator");

    @Ignore
    public static final String ID = "ID";

    @Ignore
    public static final String NAME = "NAME";

    @Ignore
    public static final String IDCASA = "IDCASA";

    @Ignore
    public static final String IMAGEN = "IMAGEN";

    @Ignore
    public static final String DESCRIPTION = "DESCRIPTION";

    @Ignore
    public static final String USERID = "USERID";

    @PrimaryKey
    @NonNull
    private String id;

    @ColumnInfo(name = "name")
    private String name = new String();

    @ColumnInfo(name = "idcasa")
    private String idCasa = new String();

    @ColumnInfo(name = "imagen")
    private String imagen = new String();

    @ColumnInfo(name = "description")
    private String description = new String();

    @ColumnInfo(name = "userid")
    private String userId = new String();

    @Ignore
    public Caja(String name, String idCasa, String description, String userId) {
        this.name = name;
        this.idCasa = idCasa;
        this.description = description;
        this.userId = userId;
    }


    public Caja(String id, String name, String idCasa, String description, String userId,String imagen) {
        this.id = id;
        this.name = name;
        this.idCasa = idCasa;
        this.description = description;
        this.userId = userId;
        this.imagen = imagen;
    }

    @Ignore
    Caja(Intent intent) {
        id = intent.getStringExtra(Caja.ID);
        name = intent.getStringExtra(Caja.NAME);
        idCasa = intent.getStringExtra(Caja.DESCRIPTION);
        description = intent.getStringExtra(Caja.DESCRIPTION);
        userId = intent.getStringExtra(Caja.USERID);
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getName() { return  name; }

    public void setName(String name) { this.name = name; }

    public String getIdCasa() { return idCasa; }

    public void setIdCasa(String idCasa) { this.idCasa = idCasa; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public String getUserId() { return userId; }

    public void setUserId(String userId) { this.userId = userId; }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public static void packageIntent (Intent intent, String name, String idCasa, String description, String userId, String image) {
        intent.putExtra(Caja.NAME, name);
        intent.putExtra(Caja.IDCASA, idCasa);
        intent.putExtra(Caja.DESCRIPTION, description);
        intent.putExtra(Caja.USERID, userId);
        intent.putExtra(Caja.IMAGEN,image);
    }

    public String toString() {
        return  id + ITEM_SEP + name + ITEM_SEP + idCasa + ITEM_SEP + description + ITEM_SEP + userId;
    }

    public String toLog() {
        return "ID: " + id + ITEM_SEP + " Nombre: " + name + ITEM_SEP + " IdCasa: " + idCasa + ITEM_SEP +
                " Descripción: " + description + ITEM_SEP + " UserId: " + userId;
    }
}