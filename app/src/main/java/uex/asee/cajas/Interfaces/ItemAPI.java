package uex.asee.cajas.Interfaces;

import uex.asee.cajas.Models.Item;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ItemAPI {
    @GET("objeto")
    Call<List<Item>> getItems(@Header("auth-token") String authtoken);

    @GET("objeto/id")
    public Call<Item> getItem(@Header("auth-token") String authtoken,@Query("id") String id);

    @Multipart
    @POST("objeto")
    Call<Item> createItem(@Header("auth-token") String authtoken,
                          @Part("name") RequestBody name,
                          @Part("description") RequestBody  description,
                          @Part MultipartBody.Part image,
                          @Part("idcaja") RequestBody  idcaja,
                          @Part("categoria") RequestBody  categoria);

    @DELETE("objeto")
    Call <ResponseBody> deleteItem(@Header("auth-token")String authtoken, @Query("id") String id);

    @Multipart
    @PUT("objeto")
    Call<Item> updateItem(@Header("auth-token") String authtoken,@Query("id") String id,
                          @Part("name") RequestBody name,
                          @Part("description") RequestBody  description,
                          @Part MultipartBody.Part image,
                          @Part("idcaja") RequestBody  idcaja,
                          @Part("categoria") RequestBody  categoria);
}
