package uex.asee.cajas;

import static org.junit.Assert.assertEquals;

import uex.asee.cajas.Models.Token;
import uex.asee.cajas.Models.User;

import org.junit.Test;

public class TokenUnitTest {

    @Test
    public void shouldGetUsernameOfUser() {
        Token token = new Token();
        token.setToken("token");
        assertEquals("token",token.getToken());
    }
}
