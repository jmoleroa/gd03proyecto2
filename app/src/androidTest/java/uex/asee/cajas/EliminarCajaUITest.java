package uex.asee.cajas;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressBack;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import static org.junit.Assert.assertTrue;

import androidx.test.ext.junit.rules.ActivityScenarioRule;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import uex.asee.cajas.Actividades.Login;

public class EliminarCajaUITest {

    public int esNulo() {
        try{
            //Al borrar la caja creada, la gridView se queda sin elementos y lanzará una excepción de tipo
            //no_data_found si se intenta hacer click en un elemento (si no estuviera borrada la caja no la lanzaría)
            onData(Matchers.anything()).inAdapterView(withId(R.id.grid3)).atPosition(0).perform(click());
            return 1;
        }
        catch (Exception e){
            return 0;
        }
    }

    @Rule
    public ActivityScenarioRule<Login> mActivityRule =
            new ActivityScenarioRule<>(Login.class);

    @Test
    public void shouldDeleteABox() {
        //Se pulsa el botón flotante de eliminar cajas
        onView(withId(R.id.fab2)).perform(click());
        //Se selecciona la caja que se quiere eliminar
        onData(Matchers.anything()).inAdapterView(withId(R.id.griddc)).atPosition(0).perform(click());
        //Se confirma que se quiere eliminar la caja
        onView(withId(R.id.action_confirm)).perform(click());
        //Se comprueba que el primer elemento del gridView (que era la caja que se ha eliminado) es nulo
        assertTrue(esNulo() == 0);
    }

    @Before
    public void LogInAndCreateBox() {
        String testingString = "UsuarioPrueba";
        String testEmail = "usuarioPrueba@gmail.com";
        //Se introducen los parámetros para iniciar sesión correctamente
        onView(withId(R.id.username)).perform(typeText(testEmail),closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText(testingString),closeSoftKeyboard());
        //Se pulsa el botón de iniciar sesión
        onView(withId(R.id.loginButton)).perform(click());
        //Se accede al fragmento de las cajas
        onView(withId(R.id.boxes)).perform(click());
        //Se pulsa el botón flotante primero para desplegar los botones flotantes de crear o eliminar cajas
        onView(withId(R.id.fab)).perform(click());
        //Se pulsa el botón flotante de crear caja
        onView(withId(R.id.fab1)).perform(click());
        //Se introducen los datos para crear un caja
        onView(withId(R.id.nbox_name)).perform(typeText("miCaja"),closeSoftKeyboard());
        onView(withId(R.id.nbox_desc)).perform(typeText("descripcionDeMiCaja"),closeSoftKeyboard());
        //Se selecciona que se quiere crear el caja
        onView(withId(R.id.action_confirm)).perform(click());
    }

    @After
    public void WipeDataAndLogOut() {
        //Se vuelve a la pantalla principal
        onView(withId(R.id.home)).perform(click());
        //Se accede a la pantalla de ajustes
        onView(withId(R.id.ConfigButton)).perform(click());
        //Se selecciona que se quieren borrar todos los datos creados
        onView(withText("Reseteo de Fabrica")).perform(click());
        //Se pregunta si se está seguro de borrar los datos y se acepta
        onView(withText("Si")).perform(click());
        //Se pulsa el botón back para intentar cerrar la aplicación
        onView(isRoot()).perform(pressBack());
        //Se selecciona que se quiere salir de la aplicación
        onView(withText("Si")).perform(click());
    }
}
