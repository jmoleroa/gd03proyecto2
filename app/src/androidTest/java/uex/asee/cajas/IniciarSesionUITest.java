package uex.asee.cajas;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressBack;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import uex.asee.cajas.Actividades.Login;

@RunWith(AndroidJUnit4.class)
public class IniciarSesionUITest {

    @Rule
    public ActivityScenarioRule<Login> mActivityRule =
            new ActivityScenarioRule<>(Login.class);

    //Este test inicia sesión con el usuario creado en el archivo "CrearUsuarioUITest" y,
    //una vez iniciada sesión, se presiona el botón back y se cierra la aplicación seleccionando
    //que se quiere cerrar la sesión
    @Test
    public void shouldLogIn() {
        String testingString = "UsuarioPrueba";
        String testEmail = "usuarioPrueba@gmail.com";
        //Se introducen los parámetros para iniciar sesión correctamente
        onView(withId(R.id.username)).perform(typeText(testEmail),closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText(testingString),closeSoftKeyboard());
        //Se pulsa el botón de iniciar sesión
        onView(withId(R.id.loginButton)).perform(click());
        //Se pulsa el botón back para intentar cerrar la aplicación
        onView(isRoot()).perform(pressBack());
        //Se selecciona que se quiere salir de la aplicación
        onView(withText("Si")).perform(click());
    }
}
